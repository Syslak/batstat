# MAKEFILE
# @aturhor Håvard Syslak
# @date 19.12.22


OS = Linux

CC = gcc

TARGET := bin/batstat
CFLAGS := -Wall -g
LDFLAGS := $(shell pkg-config --cflags --libs gio-2.0)

SRCDIR := src
OBJDIR := obj

SRCS := $(shell find $(SRCDIR) -name "*.c")
HDRS := $(shell find $(SRCDIR) -name "*.h")
OBJS := $(SRCS:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

$(TARGET): $(OBJS) 
	@mkdir -p bin
	$(CC) $(LDFLAGS) -o $@ $^

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(HDRS)
	@mkdir -p $(shell dirname $@)
	$(CC) $($CFLAGS) $(LDFLAGS) $(@:$(OBJDIR)/%.o) -o $@ -c $<
	
clean:
	rm -f $(OBJDIR)/*.o

all: clean $(TARGET)

run: $(TARGET)
	./$(TARGET)
