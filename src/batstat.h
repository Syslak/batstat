/*
*    Project: batstat
*         __ 
*     |_|(_
*     | |__)
*    Author: Håvard Syslak
*    Date: 19.12.2022
*/

#ifndef BATSTAT_H
#define BATSTAT_H
#include <gio/gio.h>

#define BAT_STATUSFILE "/sys/class/power_supply/BAT0/status"
#define BAT_CAPACITYFILE "/sys/class/power_supply/BAT0/capacity"
//#define BAT_STATUSFILE "status"
//#define BAT_CAPACITYFILE "capacity"

#define BAT_CAPACITYFILE_ERROR 1
#define BAT_STATUSFILE_ERROR 2
#define BAT_CAPACITYFILE_PARSE_ERROR 3
#define BAT_STATUSFILE_PARSE_ERROR 4
#define BAT_SUCSESS 0

typedef struct {
    char status[255];
    int capacity;
} BatStatus;

int notify(char *headder, char *body);

#endif
