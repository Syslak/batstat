/*
 *    Project: batstat
 *         __ 
 *     |_|(_
 *     | |__)
 *    Author: Håvard Syslak
 *    Date: 19.12.2022
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "batstat.h"

int get_bat_status(BatStatus *bat);

int main(void) 
{
    while (1)
    {
        char *head;
        char *body;
        int sleep_time;
        BatStatus bat;

        switch (get_bat_status(&bat)) 
        {
            // TODO: Maybe exit on errors??
            case BAT_CAPACITYFILE_ERROR: 
                {
                    char *msg = "Batstat error reading capacity file:\n";
                    body = malloc(strlen(msg) + strlen(BAT_CAPACITYFILE) + 4);
                    strcpy(body, msg);
                    strcat(body, BAT_CAPACITYFILE);
                    notify("BATSTAT ERROR", body);
                    free(body);
                    return 1;
                    break;
                }
            case BAT_STATUSFILE_ERROR: 
                {
                    char *msg = "Batstat error reading status file:\n";
                    body = malloc(strlen(msg) + strlen(BAT_STATUSFILE) + 4);
                    strcpy(body, msg);
                    strcat(body, BAT_STATUSFILE);
                    notify("ERROR", body);
                    free(body);
                    return 1;
                    break;
                }
            case BAT_CAPACITYFILE_PARSE_ERROR: 
                {
                    char *msg = "Batstat error parsing capacity file:\n";
                    body = malloc(strlen(msg) + strlen(BAT_STATUSFILE) + 4);
                    strcpy(body, msg);
                    strcat(body, BAT_CAPACITYFILE);
                    notify("BATSTAT ERROR", body);
                    free(body);
                    return 1;
                    break;
                }
            case BAT_STATUSFILE_PARSE_ERROR:
                {
                    char *msg = "Batstat error parsing status file:\n";
                    body = malloc(strlen(msg) + strlen(BAT_STATUSFILE) + 4);
                    strcpy(body, msg);
                    strcat(body, BAT_STATUSFILE);
                    notify("BATSTATERROR", body);
                    free(body);
                    return 1;
                    break;
                }
            default:

                if (bat.capacity <= 5) sleep_time = 60;
                else if (bat.capacity <= 15) sleep_time = 240;
                else if (bat.capacity <= 25) sleep_time = 600;
                else if (bat.capacity <= 40) sleep_time = 1200;
                else sleep_time = 2400;

                if (!strcmp(bat.status, "Discharging"))
                {
                    body = malloc(255);
                    strcpy(body, "🔋: ");
                    char capacity[5];
                    sprintf(capacity, "%d", bat.capacity);
                    strcat(body, capacity);
                    strcat(body, "%");
                    notify("Battery status", body);
                    free(body);
                }
                sleep(sleep_time);
        }
    }

}

int get_bat_status(BatStatus *bat) 
{
    FILE *fp = fopen(BAT_STATUSFILE, "r");
    if (fp == NULL) 
    {
        fclose(fp);
        return BAT_STATUSFILE_ERROR;
    }

    else if (1 != fscanf(fp, "%s", bat->status))
    {
        fclose(fp);
        return BAT_STATUSFILE_PARSE_ERROR;
    }
    
    fclose(fp);
    fp = fopen(BAT_CAPACITYFILE, "r");
    if (fp == NULL) 
    {
        fclose(fp);
        return BAT_CAPACITYFILE_ERROR;
    }

    else if (1 != fscanf(fp, "%d", &bat->capacity)) 
    {
        fclose(fp);
        return BAT_CAPACITYFILE_PARSE_ERROR;
    }
    
    fclose(fp);
    return BAT_SUCSESS;
}

int notify(char *headder, char *body) 
{
    GApplication *app = g_application_new("bat.not", G_APPLICATION_DEFAULT_FLAGS);
    GNotification *not = g_notification_new(headder);
    GIcon *icon = g_themed_icon_new_with_default_fallbacks("dialog-information");
    g_application_register(app, NULL, NULL);
    g_notification_set_body(not, body);
    g_notification_set_icon(not, icon);
    g_application_send_notification(app, NULL, not);

    g_object_unref(not);
    g_object_unref(app);
    g_object_unref(icon);

    return 0;

}
